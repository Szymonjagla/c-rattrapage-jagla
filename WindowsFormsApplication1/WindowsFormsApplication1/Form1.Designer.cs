﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxAnimaux = new System.Windows.Forms.ListBox();
            this.BtnAjoutAni = new System.Windows.Forms.Button();
            this.BtnDeplacer = new System.Windows.Forms.Button();
            this.BtnCalcultot = new System.Windows.Forms.Button();
            this.BtnCalculSecteur = new System.Windows.Forms.Button();
            this.BtnQuitter = new System.Windows.Forms.Button();
            this.listBoxSecteur1 = new System.Windows.Forms.ListBox();
            this.listBoxSecteur2 = new System.Windows.Forms.ListBox();
            this.listBoxSecteur3 = new System.Windows.Forms.ListBox();
            this.lblResultat = new System.Windows.Forms.Label();
            this.txtBoxAnimal = new System.Windows.Forms.TextBox();
            this.textBoxSecteur = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxAnimaux
            // 
            this.listBoxAnimaux.FormattingEnabled = true;
            this.listBoxAnimaux.Location = new System.Drawing.Point(12, 70);
            this.listBoxAnimaux.Name = "listBoxAnimaux";
            this.listBoxAnimaux.Size = new System.Drawing.Size(176, 303);
            this.listBoxAnimaux.TabIndex = 0;
            // 
            // BtnAjoutAni
            // 
            this.BtnAjoutAni.Location = new System.Drawing.Point(52, 379);
            this.BtnAjoutAni.Name = "BtnAjoutAni";
            this.BtnAjoutAni.Size = new System.Drawing.Size(75, 23);
            this.BtnAjoutAni.TabIndex = 1;
            this.BtnAjoutAni.Text = "Ajouter";
            this.BtnAjoutAni.UseVisualStyleBackColor = true;
            this.BtnAjoutAni.Click += new System.EventHandler(this.BtnAjoutAni_Click);
            // 
            // BtnDeplacer
            // 
            this.BtnDeplacer.Location = new System.Drawing.Point(259, 142);
            this.BtnDeplacer.Name = "BtnDeplacer";
            this.BtnDeplacer.Size = new System.Drawing.Size(118, 23);
            this.BtnDeplacer.TabIndex = 2;
            this.BtnDeplacer.Text = "Déplacer";
            this.BtnDeplacer.UseVisualStyleBackColor = true;
            this.BtnDeplacer.Click += new System.EventHandler(this.BtnDeplacer_Click);
            // 
            // BtnCalcultot
            // 
            this.BtnCalcultot.Location = new System.Drawing.Point(259, 226);
            this.BtnCalcultot.Name = "BtnCalcultot";
            this.BtnCalcultot.Size = new System.Drawing.Size(118, 23);
            this.BtnCalcultot.TabIndex = 3;
            this.BtnCalcultot.Text = "Calculateur Cosmique";
            this.BtnCalcultot.UseVisualStyleBackColor = true;
            this.BtnCalcultot.Click += new System.EventHandler(this.BtnCalcultot_Click);
            // 
            // BtnCalculSecteur
            // 
            this.BtnCalculSecteur.Location = new System.Drawing.Point(259, 197);
            this.BtnCalculSecteur.Name = "BtnCalculSecteur";
            this.BtnCalculSecteur.Size = new System.Drawing.Size(118, 23);
            this.BtnCalculSecteur.TabIndex = 4;
            this.BtnCalculSecteur.Text = "Calcul Secteur";
            this.BtnCalculSecteur.UseVisualStyleBackColor = true;
            this.BtnCalculSecteur.Click += new System.EventHandler(this.BtnCalculSecteur_Click);
            // 
            // BtnQuitter
            // 
            this.BtnQuitter.Location = new System.Drawing.Point(277, 319);
            this.BtnQuitter.Name = "BtnQuitter";
            this.BtnQuitter.Size = new System.Drawing.Size(75, 23);
            this.BtnQuitter.TabIndex = 5;
            this.BtnQuitter.Text = "Quitter";
            this.BtnQuitter.UseVisualStyleBackColor = true;
            this.BtnQuitter.Click += new System.EventHandler(this.BtnQuitter_Click);
            // 
            // listBoxSecteur1
            // 
            this.listBoxSecteur1.FormattingEnabled = true;
            this.listBoxSecteur1.Location = new System.Drawing.Point(436, 70);
            this.listBoxSecteur1.Name = "listBoxSecteur1";
            this.listBoxSecteur1.Size = new System.Drawing.Size(120, 95);
            this.listBoxSecteur1.TabIndex = 6;
            // 
            // listBoxSecteur2
            // 
            this.listBoxSecteur2.FormattingEnabled = true;
            this.listBoxSecteur2.Location = new System.Drawing.Point(436, 171);
            this.listBoxSecteur2.Name = "listBoxSecteur2";
            this.listBoxSecteur2.Size = new System.Drawing.Size(120, 95);
            this.listBoxSecteur2.TabIndex = 7;
            // 
            // listBoxSecteur3
            // 
            this.listBoxSecteur3.FormattingEnabled = true;
            this.listBoxSecteur3.Location = new System.Drawing.Point(436, 272);
            this.listBoxSecteur3.Name = "listBoxSecteur3";
            this.listBoxSecteur3.Size = new System.Drawing.Size(120, 95);
            this.listBoxSecteur3.TabIndex = 8;
            // 
            // lblResultat
            // 
            this.lblResultat.AutoSize = true;
            this.lblResultat.Location = new System.Drawing.Point(503, 152);
            this.lblResultat.Name = "lblResultat";
            this.lblResultat.Size = new System.Drawing.Size(53, 13);
            this.lblResultat.TabIndex = 9;
            this.lblResultat.Text = "Secteur 1";
            // 
            // txtBoxAnimal
            // 
            this.txtBoxAnimal.Location = new System.Drawing.Point(43, 408);
            this.txtBoxAnimal.Name = "txtBoxAnimal";
            this.txtBoxAnimal.Size = new System.Drawing.Size(100, 20);
            this.txtBoxAnimal.TabIndex = 10;
            // 
            // textBoxSecteur
            // 
            this.textBoxSecteur.Location = new System.Drawing.Point(299, 171);
            this.textBoxSecteur.Name = "textBoxSecteur";
            this.textBoxSecteur.Size = new System.Drawing.Size(29, 20);
            this.textBoxSecteur.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(256, 411);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Resultat";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(503, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Secteur 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(503, 354);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Secteur 3";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(452, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Supprimer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(436, 373);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Tout Supprimer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 475);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxSecteur);
            this.Controls.Add(this.txtBoxAnimal);
            this.Controls.Add(this.lblResultat);
            this.Controls.Add(this.listBoxSecteur3);
            this.Controls.Add(this.listBoxSecteur2);
            this.Controls.Add(this.listBoxSecteur1);
            this.Controls.Add(this.BtnQuitter);
            this.Controls.Add(this.BtnCalculSecteur);
            this.Controls.Add(this.BtnCalcultot);
            this.Controls.Add(this.BtnDeplacer);
            this.Controls.Add(this.BtnAjoutAni);
            this.Controls.Add(this.listBoxAnimaux);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxAnimaux;
        private System.Windows.Forms.Button BtnAjoutAni;
        private System.Windows.Forms.Button BtnDeplacer;
        private System.Windows.Forms.Button BtnCalcultot;
        private System.Windows.Forms.Button BtnCalculSecteur;
        private System.Windows.Forms.Button BtnQuitter;
        private System.Windows.Forms.ListBox listBoxSecteur1;
        private System.Windows.Forms.ListBox listBoxSecteur2;
        private System.Windows.Forms.ListBox listBoxSecteur3;
        private System.Windows.Forms.Label lblResultat;
        private System.Windows.Forms.TextBox txtBoxAnimal;
        private System.Windows.Forms.TextBox textBoxSecteur;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

