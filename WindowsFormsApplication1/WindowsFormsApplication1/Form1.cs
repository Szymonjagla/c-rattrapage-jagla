﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int Secteur1;
        int Secteur2;
        int Secteur3;
        int Secteurtot;
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void BtnQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnAjoutAni_Click(object sender, EventArgs e)
        {
            if (txtBoxAnimal.Text == "")
            {
                MessageBox.Show("Veuillez entrez un Animal", "Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            { listBoxAnimaux.Items.Add(txtBoxAnimal.Text);
                txtBoxAnimal.Text = "";
            }
        }

        private void BtnDeplacer_Click(object sender, EventArgs e)
        {
            if (textBoxSecteur.Text == "")
            {
                MessageBox.Show("Veuillez entrez un n° de Secteur", "Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (textBoxSecteur.Text == "1")
            {
                listBoxSecteur1.Items.Add(listBoxAnimaux.SelectedItem);
                textBoxSecteur.Text = "";
                listBoxAnimaux.Items.Remove(listBoxAnimaux.SelectedItem);
            }
            else if (textBoxSecteur.Text == "2")
            {
                listBoxSecteur2.Items.Add(listBoxAnimaux.SelectedItem);
                textBoxSecteur.Text = "";
                listBoxAnimaux.Items.Remove(listBoxAnimaux.SelectedItem);
            }
            else if (textBoxSecteur.Text == "3")
            {
                listBoxSecteur3.Items.Add(listBoxAnimaux.SelectedItem);
                textBoxSecteur.Text = "";
                listBoxAnimaux.Items.Remove(listBoxAnimaux.SelectedItem);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBoxSecteur1.Items.Remove(listBoxSecteur1.SelectedItem);
            listBoxSecteur2.Items.Remove(listBoxSecteur2.SelectedItem);
            listBoxSecteur3.Items.Remove(listBoxSecteur3.SelectedItem);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBoxSecteur1.Items.Clear();
            listBoxSecteur2.Items.Clear();
            listBoxSecteur3.Items.Clear();
            MessageBox.Show("Les animaux de tous les secteurs ont été Supprimer", "Info",
            MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void BtnCalculSecteur_Click(object sender, EventArgs e)
        {
            if (textBoxSecteur.Text == "")
            {
                MessageBox.Show("Veuillez entrez un n° de Secteur", "Error",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (textBoxSecteur.Text == "1")
            {
                Secteur1 = listBoxSecteur1.Items.Count;
                label1.Text = "" + Secteur1+" Animal(aux) dans le secteur 1";
            }
            else if (textBoxSecteur.Text == "2")
            {
                Secteur2 = listBoxSecteur2.Items.Count;
                label1.Text = "" + Secteur2 + " Animal(aux) dans le secteur 2";
            }
            else if (textBoxSecteur.Text == "3")
            {
                Secteur3 = listBoxSecteur3.Items.Count;
                label1.Text = "" + Secteur3 + " Animal(aux) dans le secteur 3";
            }
        }

        private void BtnCalcultot_Click(object sender, EventArgs e)
        {
            Secteur1 = listBoxSecteur1.Items.Count;
            Secteur2 = listBoxSecteur2.Items.Count;
            Secteur3 = listBoxSecteur3.Items.Count;
            Secteurtot = Secteur1 + Secteur2 + Secteur3;

            label1.Text = "" + Secteurtot + " Animal(aux) dans tous les secteurs";
        }
    }
}
